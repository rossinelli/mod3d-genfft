divert(-1)
define(_NNICE_, eval(_N_ & ~7))
define(_REM_, eval(_N_ - _NNICE_))

include(util.m4)
define(CPPKERNELS, `
void soa_to_aos8 (
       const float * __restrict__ const src, 
       float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int dstbase = 8 * base;

		LUNROLL(c, 0, 7, `dnl
		LUNROLL(xi, 0, 7, `dnl
		dst[eval(c + 8 * xi) + dstbase] = src[eval(xi + _N_ * c) + base];
		')')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int dstbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(c + 8 * _NNICE_) + dstbase] = src[eval(_NNICE_ + _N_ * c) + xi];
		')
	}
}

void soa_from_aos8(
       const float * __restrict__ const src, 
       float * __restrict__ const dst)
{
	float tmp[8][8];
	
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int srcbase = 8 * base;

		LUNROLL(c, 0, 7, `dnl
		LUNROLL(xi, 0, 7, `dnl
		dst[eval(xi + _N_ * c) + base] = src[eval(c + 8 * xi) + srcbase];
		')')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int srcbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(_NNICE_ + _N_ * c) + xi] = src[eval(c + 8 * _NNICE_) + srcbase];
		')
	}
}
') #end of CPPKERNELS

define(AVXKERNELS, `

#include <immintrin.h>

void soa_to_aos8 (
const float * __restrict__ const src, 
float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE_; base += 8)
	{
		LUNROLL(c, 0, 7, `dnl
		__m256 TMP(row, c) = _mm256_loadu_ps(&src[base + eval(_N_ * c)]);
		')dnl

		TR8x8(row_0, row_1, row_2, row_3, row_4, row_5, row_6, row_7);
		
		const int dstbase = 8 * base;

		LUNROLL(xi, 0, 7, `dnl
		_mm256_store_ps(&dst[eval(8 * xi) + dstbase], TMP(row, xi));
		') 
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int dstbase = 8 * xi;
		LUNROLL(c, 0, 7, `dnl
		dst[eval(c + 8 * _NNICE_) + dstbase] = src[eval(_NNICE_ + _N_ * c) + xi];
		')
	}
}

void soa_from_aos8 (
const float * __restrict__ const src, 
float * __restrict__ const dst)
{
	for(int base = 0; base < _NNICE_; base += 8)
	{
		const int srcbase = 8 * base;

		LUNROLL(xi, 0, 7, `dnl
		__m256 TMP(row, xi) = _mm256_load_ps(&src[eval(8 * xi) + srcbase]);
		')dnl

		TR8x8(row_0, row_1, row_2, row_3, row_4, row_5, row_6, row_7);
		
		LUNROLL(c, 0, 7, `dnl
		_mm256_storeu_ps(&dst[eval(_N_ * c) + base], TMP(row, c));
		')
	}

	for(int xi = 0; xi < _REM_; ++xi)
	{
		const int srcbase = 8 * xi;

		LUNROLL(c, 0, 7, `dnl
		dst[eval(_NNICE_ + _N_ * c) + xi] = src[eval(c + 8 * _NNICE_) + srcbase];
		')
	}
}
') #end of AVX KERNELS

divert(0)
ifelse(_AVX_, 1, AVXKERNELS, CPPKERNELS)

