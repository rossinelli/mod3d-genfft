enum { N = _N_, NH = N / 2, n = (N + 2) / 2, nm2 = n - 2 };

void unzip1 (
    const float * __restrict__ const in,
    float * __restrict__ const out0,
    float * __restrict__ const out1)
{
    for(int i = 0; i < NH; ++i)
    {
	out0[i] = in[0 + 2 * i];
	out1[i] = in[1 + 2 * i];
    }
}

#ifdef _AVX_
#include <immintrin.h>
#define TR8x8(v0, v1, v2, v3, v4, v5, v6, v7)			\
    do								\
    {								\
	__m256 __t0 = _mm256_unpacklo_ps(v0, v1);		\
	__m256 __t1 = _mm256_unpackhi_ps(v0, v1);		\
	__m256 __t2 = _mm256_unpacklo_ps(v2, v3);		\
	__m256 __t3 = _mm256_unpackhi_ps(v2, v3);		\
	__m256 __t4 = _mm256_unpacklo_ps(v4, v5);		\
	__m256 __t5 = _mm256_unpackhi_ps(v4, v5);		\
	__m256 __t6 = _mm256_unpacklo_ps(v6, v7);		\
	__m256 __t7 = _mm256_unpackhi_ps(v6, v7);		\
								\
	__m256 __tt0 = _mm256_shuffle_ps(__t0,__t2,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt1 = _mm256_shuffle_ps(__t0,__t2,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt2 = _mm256_shuffle_ps(__t1,__t3,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt3 = _mm256_shuffle_ps(__t1,__t3,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt4 = _mm256_shuffle_ps(__t4,__t6,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt5 = _mm256_shuffle_ps(__t4,__t6,		\
					 _MM_SHUFFLE(3,2,3,2)); \
	__m256 __tt6 = _mm256_shuffle_ps(__t5,__t7,		\
					 _MM_SHUFFLE(1,0,1,0)); \
	__m256 __tt7 = _mm256_shuffle_ps(__t5,__t7,		\
					 _MM_SHUFFLE(3,2,3,2)); \
								\
	v0 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);	\
	v1 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);	\
	v2 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);	\
	v3 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);	\
	v4 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);	\
	v5 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);	\
	v6 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);	\
	v7 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);	\
    }								\
    while(0)
#endif

void unzip8 (
    const float * __restrict__ const in,
    float * __restrict__ const out0,
    float * __restrict__ const out1)
{
#ifdef _AVX_
    for(int srcbase = 0; srcbase < N; srcbase += 8)
    {
	__m256 r0 = _mm256_loadu_ps(in + srcbase + 0 * N);
	__m256 r1 = _mm256_loadu_ps(in + srcbase + 1 * N);
	__m256 r2 = _mm256_loadu_ps(in + srcbase + 2 * N);
	__m256 r3 = _mm256_loadu_ps(in + srcbase + 3 * N);
	__m256 r4 = _mm256_loadu_ps(in + srcbase + 4 * N);
	__m256 r5 = _mm256_loadu_ps(in + srcbase + 5 * N);
	__m256 r6 = _mm256_loadu_ps(in + srcbase + 6 * N);
	__m256 r7 = _mm256_loadu_ps(in + srcbase + 7 * N);

	TR8x8(r0, r1, r2, r3, r4, r5, r6, r7);
    
	const int dstbase = 4 * srcbase;

	_mm256_storeu_ps(out0 + dstbase + 0, r0);
	_mm256_storeu_ps(out0 + dstbase + 8, r2);
	_mm256_storeu_ps(out0 + dstbase + 16, r4);
	_mm256_storeu_ps(out0 + dstbase + 24, r6);

	_mm256_storeu_ps(out1 + dstbase + 0, r1);
	_mm256_storeu_ps(out1 + dstbase + 8, r3);
	_mm256_storeu_ps(out1 + dstbase + 16, r5);
	_mm256_storeu_ps(out1 + dstbase + 24, r7);
    }
#else
    for(int i = 0; i < NH; ++i)
	for(int c = 0; c < 8; ++c)
	    out0[c + 8 * i] = in[0 + 2 * i + N * c];

    for(int i = 0; i < NH; ++i)
	for(int c = 0; c < 8; ++c)
	    out1[c + 8 * i] = in[1 + 2 * i + N * c];
#endif
}

#include <math.h>

void zip8_modulus (
    const float * __restrict__ const rin,
    const float * __restrict__ const iin,
    float * __restrict__ const out)
{
    for(int i = 0; i < n; ++i)
	for(int c = 0; c < 8; ++c)
	{
	    const float re = rin[c + 8 * i];
	    const float im = iin[c + 8 * i];

	    const float r2 = re * re + im * im;

#ifdef _FAST_
	    out[i + n * c] = sqrtf(fmaxf(1e-7f, r2));
#else
	    out[i + n * c] = sqrtf(r2);
#endif
	}
}
