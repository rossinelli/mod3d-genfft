#include <math.h>

void zip8_modulus (
    const float * __restrict__ const rin,
    const float * __restrict__ const iin,
    float * __restrict__ const out);

void unzip1 (
    const float * __restrict__ const in,
    float * __restrict__ const out0,
    float * __restrict__ const out1);

void unzip8 (
    const float * __restrict__ const in,
    float * __restrict__ const out0,
    float * __restrict__ const out1);

void soa_to_aos8 (
    const float * __restrict__ const src,
    float * __restrict__ const dst);

void soa_from_aos8(
    const float * __restrict__ const src,
    float * __restrict__ const dst);

void transpose_xy_N (
    float * __restrict__ const data);

void transpose_xz_N (
    float * __restrict__ const data);

void transpose_xz_n (
    float * __restrict__ const data);

void r2c8 (
    float * R0,
    float * R1,
    float * Cr,
    float * Ci);

void r2c1 (
    float * R0,
    float * R1,
    float * Cr,
    float * Ci);

void c2c8 (
    const float * ri,
    const float * ii,
    float * ro,
    float * io);

void c2c1 (
    const float * ri,
    const float * ii,
    float * ro,
    float * io);

enum
{
    N = _N_,
    NH = N / 2,
    N2 = N * N,
    n = (N + 2) / 2,
    n2 = n * n,
    n3 = n * n2,
    nnice = n & ~7,
    nm1 = n - 1,
    nm2 = n - 2,
    nm1nice = 1 + (nm2 & ~7)
};

static void modulus(
    const float * __restrict__ const rin,
    const float * __restrict__ const iin,
    float * __restrict__ const out)
{
    for(int i = 0; i < n; ++i)
    {
	const float re = rin[i];
	const float im = iin[i];
	const float r2 = re * re + im * im;

#ifdef _FAST_
	out[i] = sqrtf(fmaxf(1e-7f, r2));
#else
	out[i] = sqrtf(r2);
#endif
    }
}

static void boundary_line(float * data)
{
    float R0[NH], R1[NH];

    unzip1(data, R0, R1);

    r2c1(R0, R1, data, data + NH);
}

static void r2c1_modulus (
    const float * __restrict__ const src,
    float * __restrict__ const dst)
{
    float __attribute__((aligned(64))) R0[n], R1[n];

    unzip1(src, R0, R1);
    r2c1(R0, R1, R0, R1);

    R1[0] = 0;
    R1[nm1] = 0;

    modulus(R0, R1, dst);
}

static float * boundary_slice(
    const float * __restrict__ const in,
    float * __restrict__ out)
{
    /* first line is an R2C */
    r2c1_modulus(in, out);
    out += n;

    /* C2C all the other lines, except for the last one */
    {
	const float * __restrict__ const rbase = in;
	const float * __restrict__ const ibase = in + N * NH;

	float __attribute__((aligned(64))) R[N * 8], I[N * 8];

	for(int y = 1; y < nm1nice; y += 8)
	{
	    const float * __restrict__ const rlines = rbase + N * y;
	    const float * __restrict__ const ilines = ibase + N * y;

	    soa_to_aos8(rlines, R);
	    soa_to_aos8(ilines, I);

	    c2c8(R, I, R, I);

	    zip8_modulus(R, I, out);
	    out += 8 * n;
	}

	for(int y = nm1nice; y < nm1; ++y)
	{
	    const float * __restrict__ const rline = rbase + N * y;
	    const float * __restrict__ const iline = ibase + N * y;

	    c2c1(rline, iline, R, I);

	    modulus(R, I, out);
	    out += n;
	}
    }

    /* last line is an R2C */
    r2c1_modulus(in + N * NH, out);
    out += n;

    return out;
}

__attribute__ ((visibility ("default")))
int mod3d_n() { return n; }

__attribute__ ((visibility ("default")))
int mod3d_N() { return N; }

__attribute__ ((visibility ("default")))
void mod3d (float * inout)
{
    for(int z = 0; z < N; ++z)
    {
	float * __restrict__ const slice = inout + N2 * z;

	/* R2C transform along x-direction */
	{
	    float __attribute__((aligned(64))) tmp[N * 8];

	    for(int y = 0; y < N; y += 8)
	    {
		float * __restrict__ const lines = slice + N * y;

		enum { NH8 = NH * 8 };
		unzip8(lines, tmp, tmp + NH8);

		/* this line below is not so obvious */
		r2c8(tmp, tmp + NH8, tmp, tmp + NH8);

		soa_from_aos8(tmp, lines);
	    }
	}

	transpose_xy_N(slice);

	/* C2C and occasional R2C in the y-direction */
	{
	    /* first line does not have an imaginary counterpart */
	    boundary_line(slice);

	    /* C2C lines */
	    {
		float * __restrict__ const rbase = slice;
		float * __restrict__ const ibase = slice + N * NH;

		float __attribute__((aligned(64))) R[N * 8], I[N * 8];

		for(int y = 1; y < nm1nice; y += 8)
		{
		    float * __restrict__ const rlines = rbase + N * y;
		    float * __restrict__ const ilines = ibase + N * y;

		    soa_to_aos8(rlines, R);
		    soa_to_aos8(ilines, I);

		    c2c8(R, I, R, I);

		    soa_from_aos8(R, rlines);
		    soa_from_aos8(I, ilines);
		}

		for(int y = nm1nice; y < nm1; ++y)
		{
		    float * __restrict__ const rline = rbase + N * y;
		    float * __restrict__ const iline = ibase + N * y;

		    c2c1(rline, iline, rline, iline);
		}
	    }

	    /* the last line does not have an imaginary counterpart */
	    boundary_line(slice + N * NH);
	}

	transpose_xy_N(slice);
    }

    transpose_xz_N(inout);

    float * dst = inout;

    /* C2C across the z-direction and unfrequently some R2C
     * also, I compute the modulus on the fly */
    {
	dst = boundary_slice(inout, dst);

	for(int z = 1; z < nm1; ++z)
	{
	    const float * __restrict__ const rslice = inout + N2 * (0 + z);
	    const float * __restrict__ const islice = inout + N2 * (NH + z);

	    float __attribute__((aligned(64))) R[N * 8], I[N * 8];

	    for(int y = 0; y < nnice; y += 8)
	    {
		const float * __restrict__ const rlines = rslice + N * y;
		const float * __restrict__ const ilines = islice + N * y;

		soa_to_aos8(rlines, R);
		soa_to_aos8(ilines, I);

		c2c8(R, I, R, I);

		zip8_modulus(R, I, dst);
		dst += 8 * n;
	    }

	    for(int y = nnice; y < n; ++y)
	    {
		const float * __restrict__ const rline = rslice + N * y;
		const float * __restrict__ const iline = islice + N * y;

		c2c1(rline, iline, R, I);

		modulus(R, I, dst);
		dst += n;
	    }
	}

	dst = boundary_slice(inout + N2 * NH, dst);
    }

    transpose_xz_n(inout);
}
