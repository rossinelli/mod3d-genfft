divert(-1)
include(util.m4)

define(_NNICE_, eval(_N_ & ~7))
define(_NM1_, eval(_N_ - 1))

define (MACROS_CPP, ``
enum { TILE = 8, TILE2 = TILE * TILE };

#define LOAD_TRANSPOSED(in, out, STRIDE)			\
    do								\
    {								\
	float __attribute__((aligned(64))) staging[TILE2];	\
								\
	for(int j = 0; j < TILE; ++j)				\
	{							\
	    const float * __restrict__ const s =		\
		in + STRIDE * j;				\
								\
	    float * __restrict__ const d =			\
		staging + TILE * j;				\
								\
	    for(int i = 0; i < TILE; ++i)			\
		d[i] = s[i];					\
	}							\
								\
	for(int j = 0; j < TILE; ++j)				\
	    for(int i = 0; i < TILE; ++i)			\
		out[i + TILE * j] = staging[j + TILE * i];	\
								\
    }								\
    while(0)

#define WRITE(in, out, STRIDE)					\
    do								\
    {								\
	for(int j = 0; j < TILE; ++j)				\
	{							\
	    const float * __restrict__ const s = in + TILE * j;	\
	    float * __restrict__ const d = out + STRIDE * j;	\
								\
	    for(int i = 0; i < TILE; ++i)			\
		d[i] = s[i];					\
	}							\
    }								\
    while(0)
'')

define(MACROS_AVX, ``
enum { TILE = 8, TILE2 = TILE * TILE };

#include <immintrin.h>

#define LOAD_TRANSPOSED(in, out, STRIDE)			\
    do								\
    {								\
	__m256 reg0 = _mm256_loadu_ps(in + STRIDE * 0);		\
	__m256 reg1 = _mm256_loadu_ps(in + STRIDE * 1);		\
	__m256 reg2 = _mm256_loadu_ps(in + STRIDE * 2);		\
	__m256 reg3 = _mm256_loadu_ps(in + STRIDE * 3);		\
	__m256 reg4 = _mm256_loadu_ps(in + STRIDE * 4);		\
	__m256 reg5 = _mm256_loadu_ps(in + STRIDE * 5);		\
	__m256 reg6 = _mm256_loadu_ps(in + STRIDE * 6);		\
	__m256 reg7 = _mm256_loadu_ps(in + STRIDE * 7);		\
								\
	TR8x8(reg0, reg1, reg2, reg3, reg4, reg5, reg6, reg7);	\
								\
	_mm256_store_ps(out + 8 * 0, reg0);			\
	_mm256_store_ps(out + 8 * 1, reg1);			\
	_mm256_store_ps(out + 8 * 2, reg2);			\
	_mm256_store_ps(out + 8 * 3, reg3);			\
	_mm256_store_ps(out + 8 * 4, reg4);			\
	_mm256_store_ps(out + 8 * 5, reg5);			\
	_mm256_store_ps(out + 8 * 6, reg6);			\
	_mm256_store_ps(out + 8 * 7, reg7);			\
    }								\
    while(0)

#define WRITE(in, out, STRIDE)					\
	do							\
	{							\
	    _mm256_storeu_ps(out + STRIDE * 0,			\
			    _mm256_load_ps(in + 8 * 0));	\
	    _mm256_storeu_ps(out + STRIDE * 1,			\
			    _mm256_load_ps(in + 8 * 1));	\
	    _mm256_storeu_ps(out + STRIDE * 2,			\
			    _mm256_load_ps(in + 8 * 2));	\
	    _mm256_storeu_ps(out + STRIDE * 3,			\
			    _mm256_load_ps(in + 8 * 3));	\
	    _mm256_storeu_ps(out + STRIDE * 4,			\
			    _mm256_load_ps(in + 8 * 4));	\
	    _mm256_storeu_ps(out + STRIDE * 5,			\
			    _mm256_load_ps(in + 8 * 5));	\
	    _mm256_storeu_ps(out + STRIDE * 6,			\
			    _mm256_load_ps(in + 8 * 6));	\
	    _mm256_storeu_ps(out + STRIDE * 7,			\
			    _mm256_load_ps(in + 8 * 7));	\
	}							\
	while(0)
'')
divert(0)

ifelse(_AVX_, 1, MACROS_AVX, MACROS_CPP)

enum { N = _N_, NNICE = N & ~7, N2 = N * N };

void `transpose_xy_N' (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2];

    for(int y = 0; y < NNICE; y += TILE)
    {
	{
	    float * __restrict__ const r0 = data + y + N * y;

	    LOAD_TRANSPOSED(r0, tmp0, N);

	    WRITE(tmp0, r0, N);
	}

	float * __restrict__ const base0 = data + N * y;
	float * __restrict__ const base1 = data + y;

	for(int x = y + TILE; x < NNICE; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + N * x;

	    LOAD_TRANSPOSED(r0, tmp0, N);
	    LOAD_TRANSPOSED(r1, tmp1, N);

	    WRITE(tmp0, r1, N);
	    WRITE(tmp1, r0, N);
	}
    }

    /* THIS CODE HANDLES ONLY SIZES 8 * X */
}

void `transpose_xz_N' (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2];

    for(int z = 0; z < NNICE; z += TILE)
    {
	float * __restrict__ const base0 = data + N2 * z;
	float * __restrict__ const base1 = data + z;

	for(int y = 0; y < N; ++y)
	{
	    float * __restrict__ const r0 = base0 + z + N * y;

	    LOAD_TRANSPOSED(r0, tmp0, N2);

	    WRITE(tmp0, r0, N2);
	}

	for(int x = z + TILE; x < NNICE; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + N2 * x;

	    for(int y = 0; y < N; ++y)
	    {
		float * __restrict__ const t0 = r0 + N * y;
		float * __restrict__ const t1 = r1 + N * y;

		LOAD_TRANSPOSED(t0, tmp0, N2);
		LOAD_TRANSPOSED(t1, tmp1, N2);

		WRITE(tmp0, t1, N2);
		WRITE(tmp1, t0, N2);
	    }
	}
    }
}

enum { n = (N + 2) / 2, n2 = n * n, nnice = n & ~7};

void `transpose_xz_n' (float * __restrict__ const data)
{
    float __attribute__((aligned(64))) tmp0[TILE2], tmp1[TILE2];

    for(int z = 0; z < nnice; z += TILE)
    {
	float * __restrict__ const base0 = data + n2 * z;
	float * __restrict__ const base1 = data + z;

	for(int y = 0; y < n; ++y)
	{
	    float * __restrict__ const r0 = base0 + z + n * y;

	    LOAD_TRANSPOSED(r0, tmp0, n2);

	    WRITE(tmp0, r0, n2);
	}

	for(int x = z + TILE; x < nnice; x += TILE)
	{
	    float * __restrict__ const r0 = base0 + x;
	    float * __restrict__ const r1 = base1 + n2 * x;

	    for(int y = 0; y < n; ++y)
	    {
		float * __restrict__ const t0 = r0 + n * y;
		float * __restrict__ const t1 = r1 + n * y;

		LOAD_TRANSPOSED(t0, tmp0, n2);
		LOAD_TRANSPOSED(t1, tmp1, n2);

		WRITE(tmp0, t1, n2);
		WRITE(tmp1, t0, n2);
	    }
	}
    }

    /* handling arbitrary n */
    for(int z = nnice; z < n; ++z)
        for(int x = 0; x < z; ++x)
            for(int y = 0; y < n; ++y)
            {
                const float t0 = data[x + n * (y + n * z)];
                const float t1 = data[z + n * (y + n * x)];

                data[x + n * (y + n * z)] = t1;
                data[z + n * (y + n * x)] = t0;
            }
}
