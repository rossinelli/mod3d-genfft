define(`forloop',
       `pushdef(`$1', `$2')_forloop(`$1', `$2', `$3', `$4')popdef(`$1')')

define(`_forloop',
       `$4`'ifelse($1, `$3', ,
		   `define(`$1', incr($1))_forloop(`$1', `$2', `$3', `$4')')')

define(`forrloop',
       `pushdef(`$1', `$2')_forrloop(`$1', `$2', `$3', `$4')popdef(`$1')')

define(`_forrloop',
       `$4`'ifelse($1, `$3', ,
		   `define(`$1', decr($1))_forrloop(`$1', `$2', `$3', `$4')')')

USAGE LUNROLL
$1 iteration variable
$2 iteration start
$3 iteration end
$4 body

define(LUNROLL, `forloop($1, $2, $3,`$4')')
define(RLUNROLL, `forrloop($1, $2, $3, `$4')')
define(`TMP', $1_$2)

define(`REDUCEL',`
ifelse(eval($# < 3), 1,, $2 = $1($2, $3);)' `ifelse(eval($# <= 3), 1,`',`REDUCEL($1, shift(shift(shift($*))))')')

define(`ODDREMOVE', `ifelse(eval($# <= 2), 1, ifelse(eval($# > 0),1,$1), `$1,ODDREMOVE(shift(shift($*)))')')

define(`REDUCE',`REDUCEL($*)' `ifelse(eval($# <= 3), 1, ,`
REDUCE($1, ODDREMOVE(shift($*)))')')

#example: REDUCE(`+=', s0, s1, s2, s3, s4, s5, s6, s7)

define(`TR8x8', `do		\
    {		\
	__m256 __t0 = _mm256_unpacklo_ps($1, $2);\
	__m256 __t1 = _mm256_unpackhi_ps($1, $2);\
	__m256 __t2 = _mm256_unpacklo_ps($3, $4);\
	__m256 __t3 = _mm256_unpackhi_ps($3, $4);\
	__m256 __t4 = _mm256_unpacklo_ps($5, $6);\
	__m256 __t5 = _mm256_unpackhi_ps($5, $6);\
	__m256 __t6 = _mm256_unpacklo_ps($7, $8);\
	__m256 __t7 = _mm256_unpackhi_ps($7, $8);\
\
	__m256 __tt0 = _mm256_shuffle_ps(__t0,__t2,\
					 _MM_SHUFFLE(1,0,1,0));\
	__m256 __tt1 = _mm256_shuffle_ps(__t0,__t2,\
					 _MM_SHUFFLE(3,2,3,2));\
	__m256 __tt2 = _mm256_shuffle_ps(__t1,__t3,\
					 _MM_SHUFFLE(1,0,1,0));\
	__m256 __tt3 = _mm256_shuffle_ps(__t1,__t3,\
					 _MM_SHUFFLE(3,2,3,2));\
	__m256 __tt4 = _mm256_shuffle_ps(__t4,__t6,\
					 _MM_SHUFFLE(1,0,1,0));\
	__m256 __tt5 = _mm256_shuffle_ps(__t4,__t6,\
					 _MM_SHUFFLE(3,2,3,2));\
	__m256 __tt6 = _mm256_shuffle_ps(__t5,__t7,\
					 _MM_SHUFFLE(1,0,1,0));\
	__m256 __tt7 = _mm256_shuffle_ps(__t5,__t7,\
					 _MM_SHUFFLE(3,2,3,2));\
\
	$1 = _mm256_permute2f128_ps(__tt0, __tt4, 0x20);\
	$2 = _mm256_permute2f128_ps(__tt1, __tt5, 0x20);\
	$3 = _mm256_permute2f128_ps(__tt2, __tt6, 0x20);\
	$4 = _mm256_permute2f128_ps(__tt3, __tt7, 0x20);\
	$5 = _mm256_permute2f128_ps(__tt0, __tt4, 0x31);\
	$6 = _mm256_permute2f128_ps(__tt1, __tt5, 0x31);\
	$7 = _mm256_permute2f128_ps(__tt2, __tt6, 0x31);\
	$8 = _mm256_permute2f128_ps(__tt3, __tt7, 0x31);\
    }\
    while(0)')
