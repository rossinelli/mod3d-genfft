#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

    int mod3d_n();
    
    int mod3d_N();
    
    void mod3d(float * inout);

#ifdef __cplusplus
}
#endif
