# mod3d : a 3D DFT modulus library

This library performs a backward 3D DFT and compute the modulus of a complex-valued signal, whose spectrum is real-valued.
The code is designed to achieve (more than) decent performance and it is based on genfft.

The idea is to manipulate (in-place) the N x N x N input to decompose it into n x N x N frequencies (n = (N + 2) / 2)
and compute the modulus of the first n x n x n on the fly.
![alt text][logo]

[logo]: https://github.com/rossinelli/mod3d-genfft/blob/master/sketch.png "Idea"