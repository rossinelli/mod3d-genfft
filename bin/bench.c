#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <assert.h>
#include <string.h>
#include <errno.h>

#define POSIX_CHECK(stmt, ...)				\
    do							\
    {							\
	if (!(stmt))					\
	{						\
	    fprintf(stderr,				\
		    "error: %s\n", strerror(errno));	\
							\
	    fprintf(stderr,				\
		    "during: "				\
		    #stmt);				\
							\
	    fprintf(stderr, "\n" # __VA_ARGS__		\
		    " in file* %s at line %d\n"		\
		    "\nexiting now.\n",			\
		    __FILE__, __LINE__);		\
							\
	    fflush(stderr);				\
							\
	    exit(EXIT_FAILURE);				\
	}						\
    }							\
    while(0)

#include <math.h>

#include <fftw3.h>

#define SIGMA 0.85
#define XI (3. / 4. * M_PI)

#define WRAP(x) (((x) + N) % N)
#define ENTRY(x, y, z) (WRAP(x) + N * (WRAP(y) + N * WRAP(z)))

#define DDUMP(spat)						\
    do								\
    {								\
	char pathname[512];					\
								\
	static int idcall = 0;					\
								\
	sprintf(pathname, spat, idcall++);			\
								\
	FILE * f;						\
	POSIX_CHECK(f = fopen(pathname, "wb"));			\
	POSIX_CHECK(N3 == fwrite(ddbuf, sizeof(float), N3, f));	\
	POSIX_CHECK(0 == fclose(f));				\
    }								\
    while(0)

#define CHECK(stmt, ...)			\
    do						\
    {						\
	if (!(stmt))				\
	{					\
	    fprintf(stderr,			\
		    __VA_ARGS__);		\
						\
	    exit(EXIT_FAILURE);			\
	}					\
    }						\
    while(0)

enum
{
    n = 65,
    n2 = n * n,
    n3 = n2 * n,
    N = 2 * n - 2,
    NH = N / 2,
    N2 = N * N,
    N3 = N2 * N,
    ntimes = 1
};

const int verbose = 1;

fftwf_plan dftplan = 0;
fftwf_complex * __restrict__ dft = 0;

static void cook_kernel(
    float xdir,
    float ydir,
    float zdir,
    const float radius,
    float * const __restrict__ rdft)
{
    const float h = radius * 2 * M_PI / n;

    const float multiplier = XI * h;

    xdir *= multiplier;
    ydir *= multiplier;
    zdir *= multiplier;

    float * const ampls = fftwf_alloc_real(N3);

    /* compute the amplitudes */
    {
        const float multiplier = -0.5f * h * h / (SIGMA * SIGMA);

	float lut[NH + 1];

	for(int x = 0; x <= NH; ++x)
	    lut[x] = expf(multiplier * x * x);

	for(int z = 0; z <= NH; ++z)
	{
	    float * const __restrict__ slice = ampls + N2 * z;

	    const float zexp = lut[z];

	    for(int y = 0; y <= NH; ++y)
	    {
		float * const __restrict__ line = slice + N * y;

		const float yzexp = lut[y] * zexp;

		for(int x = 0; x <= NH; ++x)
		{
#ifndef _RANDOM_IC_
		    line[x] = lut[abs(x)] * yzexp;
#else
		    line[x] += 1e-3 * (pow((N - x) * 0.1, 3)/ pow(NH - fabs(NH/2 - y) * 0.95, 2.5)) * pow(NH - fabs(NH/2 - z), 1.4);
		    line[x] += 0.25 * (drand48() - 0.5);
#endif
		}

		for(int x = NH + 1; x < N; ++x)
		    line[x] = line[N - x];

		if (y > 0 && y < NH)
		    memcpy(slice + N * (N - y), line, sizeof(float) * N);
	    }

	    if (z > 0 && z < NH)
		memcpy(ampls + N2 * (N - z), slice, sizeof(float) * N2);
	}
    }

    float * const cosines = fftwf_alloc_real(N3);

    for(int zdst = 0; zdst < N; ++zdst)
    {
	const int zpos = zdst - N * (zdst >= NH);

	const float zterm = zdir * zpos;

	float * const __restrict__ slice = cosines + N2 * zdst;

	for(int ydst = 0; ydst < N; ++ydst)
	{
	    const int ypos = ydst - N * (ydst >= NH);

	    const float yzterm = ydir * ypos + zterm;

	    float * const __restrict__ line = slice + N * ydst;

	    for(int x = 0; x < NH; ++x)
		line[x] = cosf(xdir * x + yzterm);

	    for(int x = NH; x < N; ++x)
		line[x] = cosf(xdir * (x - N) + yzterm);
	}
    }

    float beta;

    /* mkl's way of computing beta */
    {
	double num = 0;

	for(int i = 0; i < N3; ++i)
	    num += cosines[i] * ampls[i];

	float denom = 0;

	for(int i = 0; i < N3; ++i)
	    denom += ampls[i];

	beta = num / denom;

	if (verbose)
	    fprintf(stderr,
		    "HIPASS r %g (%g) direction %g %g %g beta %g, n %d N %d\n",
		    radius, radius / SIGMA, xdir, ydir, zdir, beta, n, N);
    }

    /* cook the kernel */
    for(int zdst = 0; zdst < N; ++zdst)
    {
	const int zpos = zdst - N * (zdst >= NH);

	const float zterm = zdir * zpos;

	const float * const __restrict__ aslice = ampls + N2 * zdst;
	const float * const __restrict__ cslice = cosines + N2 * zdst;

	fftwf_complex * const __restrict__ dslice = dft + N2 * zdst;

	for(int ydst = 0; ydst < N; ++ydst)
	{
	    const int ypos = ydst - N * (ydst >= NH);

	    const float yzterm = ydir * ypos + zterm;

	    const float * const aline = aslice + N * ydst;
	    const float * const cline = cslice + N * ydst;

	    fftwf_complex * const __restrict__ dline = dslice + N * ydst;

	    for(int x = 0; x < NH; ++x)
	    {
		const float ampl = aline[x];

		dline[x][0] = ampl * (cline[x] - beta);
		dline[x][1] = ampl * sinf(xdir * x + yzterm);
	    }

	    for(int x = NH; x < N; ++x)
	    {
		const float ampl = aline[x];

		dline[x][0] = ampl * (cline[x] - beta);
		dline[x][1] = ampl * sinf(xdir * (x - N) + yzterm);
	    }
	}
    }

    /* make it l1unitary */
    {
	double e = 0;

	for(int i = 0; i < N3; ++i)
	{
	    const float rval = dft[i][0];
	    const float ival = dft[i][1];

	    const float r2 = rval * rval + ival * ival;
	    e += sqrt(r2);
	}

	const float rescale = 1 / e;

	for(int i = 0; i < N3; ++i)
	{
	    dft[i][0] *= rescale;
	    dft[i][1] *= rescale;
	}
    }

    float * ddbuf = fftwf_alloc_real(N3);

    if (verbose)
    {
	for(int z = -NH; z < NH; ++z)
	    for(int y = -NH; y < NH; ++y)
		for(int x = -NH; x < NH; ++x)
		    ddbuf[x + NH + N * (y + NH + N * (z + NH))] = dft[ENTRY(x, y, z)][0];

	DDUMP("morlet.%d.r.raw");

	for(int z = -NH; z < NH; ++z)
	    for(int y = -NH; y < NH; ++y)
		for(int x = -NH; x < NH; ++x)
		    ddbuf[x + NH + N * (y + NH + N * (z + NH))] = dft[ENTRY(x, y, z)][1];

	DDUMP("morlet.%d.i.raw");
    }

    fftwf_execute(dftplan);

    if (verbose)
    {
	for(int z = -NH; z < NH; ++z)
	    for(int y = -NH; y < NH; ++y)
		for(int x = -NH; x < NH; ++x)
		    ddbuf[x + NH + N * (y + NH + N * (z + NH))] = dft[ENTRY(x, y, z)][0];

	DDUMP("dft.r.morlet.%d.raw");

	for(int z = -NH; z < NH; ++z)
	    for(int y = -NH; y < NH; ++y)
		for(int x = -NH; x < NH; ++x)
		    ddbuf[x + NH + N * (y + NH + N * (z + NH))] = dft[ENTRY(x, y, z)][1];

	DDUMP("dft.i.morlet.%d.raw");
    }

    for(int i = 0; i < N3; ++i)
	assert(fabsf(dft[i][1]) < 2e-7);

    /* copy real(dft) into rdft, ignore spourious freqs as mkl suggested */
    for(int zdst = 0; zdst < N; ++zdst)
    {
	const int zpos = zdst - N * (zdst >= NH);

	const int zterm = zpos * zpos;

	fftwf_complex * const __restrict__ srcslice = dft + N2 * zdst;

	float * const __restrict__ dstslice = rdft + N2 * zdst;

	for(int ydst = 0; ydst < N; ++ydst)
	{
	    const int ypos = ydst - N * (ydst >= NH);

	    const int yzterm = ypos * ypos + zterm;

	    fftwf_complex * const srcline = srcslice + N * ydst;

	    float * const dstline = dstslice + N * ydst;

	    enum { NH2 = NH * NH };

	    for(int x = 0; x < NH; ++x)
		dstline[x] = (x * x + yzterm < NH2) * srcline[x][0];

	    for(int x = NH; x < N; ++x)
	    {
		const int xpos = x - N;

		dstline[x] = (xpos * xpos + yzterm < NH2) * srcline[x][0];
	    }
	}
    }

    if (verbose)
    {
	for(int z = -NH; z < NH; ++z)
	    for(int y = -NH; y < NH; ++y)
		for(int x = -NH; x < NH; ++x)
		    ddbuf[x + NH + N * (y + NH + N * (z + NH))] = rdft[ENTRY(x, y, z)];

	DDUMP("rdft.morlet.%d.raw");
    }

    fftwf_free(ddbuf);
    fftwf_free(cosines);
    fftwf_free(ampls);
}

unsigned long long rdtsc(void)
{
    unsigned hi, lo;
    __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
    return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

double bench_ref(
    const float * __restrict__ const kernel,
    float * __restrict__ const output)
{
    for(int z = 0; z < N; ++z)
    {
	const float * __restrict__ const kslice = kernel + N2 * z;

	fftwf_complex * __restrict__ const dstslice = dft + N2 * z;

	for(int y = 0; y < N; ++y)
	{
	    const float * __restrict__ const kline = kslice + N * y;

	    fftwf_complex * __restrict__ const dstline = dstslice + N * y;

	    for(int x = 0; x < N; ++x)
	    {
		dstline[x][0] = kline[x];
		dstline[x][1] = 0;
	    }
	}
    }

    const uint64_t t0 = rdtsc();

    fftwf_execute(dftplan);

    for(int z = 0; z < n; ++z)
    {
	fftwf_complex * __restrict__ const srcslice = dft + N2 * z;

	float * __restrict__ const dstslice = output + n2 * z;

	for(int y = 0; y < n; ++y)
	{
	    fftwf_complex * __restrict__ const srcline = srcslice + N * y;

	    float * __restrict__ const dstline = dstslice + n * y;

	    for(int x = 0; x < n; ++x)
	    {
		const float rval = srcline[x][0];
		const float ival = srcline[x][1];

		const float r2 = rval * rval + ival * ival;

#ifdef _FAST_
		dstline[x] = sqrtf(fmaxf(1e-7f, r2));
#else
		dstline[x] = sqrtf(r2);
#endif
	    }
	}
    }

    const uint64_t t1 = rdtsc();

    const double retval = (double)(t1 - t0);
    printf("BASELINE took %.4e C\n", retval);


    {
	FILE * f = fopen("ref.raw", "wb");
	fwrite(output, sizeof(float), n3, f);
	fclose(f);
    }

    return retval;
}

#include <mod3d.h>

double bench (
    float * __restrict__ const kernel,
    float * __restrict__ const output)
{
    CHECK(mod3d_n() == n,
	  "error: wrong in libmod3d n(%d instead of %d)\n",
	  mod3d_n(), n);

    CHECK(mod3d_N() == N,
	  "error: wrong in libmod3d N(%d instead of %d)\n",
	  mod3d_N(), N);

    const uint64_t t0 = rdtsc();

    for(int i = 0; i < ntimes; ++i)
	mod3d(kernel);

    memcpy(output, kernel, sizeof(float) * n3);

    const uint64_t t1 = rdtsc();

    const double retval = (double)(t1 - t0);
    printf("MOD3D took %.4e C\n", retval);

    {
	FILE * f = fopen("res.raw", "wb");
	fwrite(output, sizeof(float), n3, f);
	fclose(f);
    }

    return retval;
}

void checkacc(
    const float * ref, 
    const float * res, 
    const int N, 
    const double tol)
{
    double linf_e = 0, l1_e = 0, 
	linf_ref = 0, l1_ref = 0;

    for(int i = 0; i < N; ++i)
    {
	assert(!isnan(ref[i]));
	assert(!isnan(res[i]));

	const double err = ref[i] - res[i];
	const double maxval = fmax(fabs(res[i]), fabs(ref[i]));
	const double relerr = err / fmax(1e-6, maxval);

	if (fabs(relerr) >= tol && fabs(err) >= tol)
	    printf("%d: %e ref: %e -> %e %e\n", i, res[i], ref[i], err, relerr);

	assert(fabs(relerr) < tol || fabs(err) < tol);

	linf_e = fmax(linf_e, fabs(err));
	l1_e += fabs(err);

	linf_ref = fmax(linf_ref, fabs(ref[i]));
	l1_ref += fabs(ref[i]);
    }

    const double linf_erel = linf_e / linf_ref;
    const double l1_erel = l1_e / l1_ref;

    printf("l-infinity errors: %.03e (absolute) %.03e (relative)\n", linf_e, linf_erel);
    printf("       l-1 errors: %.03e (absolute) %.03e (relative)\n", l1_e, l1_erel);
}

int main()
{
    printf("Benchmark is N is %d\n", N);
    dft = fftwf_alloc_complex(N3);

    dftplan = fftwf_plan_dft_3d(N, N, N,
				dft, dft,
				FFTW_FORWARD, FFTW_ESTIMATE);

    CHECK(dftplan != 0,
	  "error: dftplan is NULL\n");

    const int _Q_ = 4;
    const int _J_ = 5;

    const int ir = 0;
    const float e = (_J_ - 3.f) * (_Q_ - 1.f - ir) / (_Q_ - 1.f);
    const float r = SIGMA * n * powf(0.5f, 3 + e);

    float * rdft = fftwf_alloc_real(N3);

    cook_kernel(
	8.638463e-01, 1.080926e-01, 4.920219e-01, r, rdft);


    fftwf_destroy_plan(dftplan);

    dftplan = fftwf_plan_dft_3d(N, N, N,
				dft, dft,
				FFTW_BACKWARD, FFTW_ESTIMATE);


    float * ref = fftwf_alloc_real(n3);

    const double tts0 = bench_ref(rdft, ref);

    float * res = fftwf_alloc_real(n3);
    float * rdft_copy = fftwf_alloc_real(N3);
    memcpy(rdft_copy, rdft, sizeof(float) * N3);

    const double tts1 = bench(rdft_copy, res);

    printf("IMPROVEMENT OVER BASELINE: %.2fX\n", tts0 / tts1);

    checkacc(ref, res, n3, 5e-4);

    fftwf_free(res);
    fftwf_free(rdft_copy);
    fftwf_free(ref);

    fftwf_free(rdft);

    fftwf_destroy_plan(dftplan);
    fftwf_free(dft);

    return EXIT_SUCCESS;
}
